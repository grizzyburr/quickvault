from cmd2 import Cmd,Statement
import restic
import os
from dotenv import load_dotenv
from restic import errors
import shutil
from sys import exit
import requests

# Binary configuration
if shutil.which('restic'):
    resticPath = shutil.which('restic')
else:
    resticPath = os.path.join(os.environ['APPDATA'], 'grizzyburr\\restic\\restic.exe')
    if os.path.exists(resticPath):
        restic.binary_path = resticPath
    else:
        print("Restic doesn't appear to be in the system path")
        print("Would you like to install a copy of restic in the appdata directory?")
        answer = input('(y/n)> ')
        if answer.lower() == 'y':
            if not os.path.exists(resticPath):
                try:
                    os.makedirs(os.path.dirname(resticPath))
                except FileExistsError:
                    pass
                response = requests.get('https://github.com/restic/restic/releases/download/v0.16.2/restic_0.16.2_windows_amd64.zip')
                unzipTarget = os.path.join(os.environ['TEMP'], 'restic.zip')
                with open(unzipTarget, 'wb') as file:
                    file.write(response.content)
                shutil.unpack_archive(unzipTarget, os.path.dirname(resticPath))
                files = os.listdir(os.path.dirname(resticPath))
                for file in files:
                    if 'restic_' in file:
                        source = os.path.join(os.path.dirname(resticPath), file)
                        shutil.move(source, resticPath)
                restic.binary_path = resticPath
        else:
            exit(1)


# Load Config
load_dotenv()

# Restic
def get_snapshots():
    snaps = restic.snapshots()
    for snap in snaps:
        returnString = f"{snap['time']} - {snap['id']} - {', '.join(snap['paths'])}"
        yield returnString

# Prompt
class prompt(Cmd):
    Cmd.prompt = '> '

    def do_backup(self, _: Statement):
        self.poutput(f'Backing up {_.args[:]}...')
        restic.backup(paths=_.argv)
        self.poutput('Done!')

    def do_snapshots(self, _):
        for snapshot in get_snapshots():
            self.poutput(snapshot)

    def do_init(self, _: Statement):
        if not os.path.exists(os.environ['RESTIC_REPOSITORY']):
            os.mkdir(os.environ['RESTIC_REPOSITORY'])
        try:
            print(f'Creating repo at {os.environ['RESTIC_REPOSITORY']}...')
            restic.init()
        except errors.ResticFailedError:
            self.poutput(f'repo already exists')

    def do_genenv(self, _):
        options = ['RESTIC_REPOSITORY','RESTIC_PASSWORD']
        env = '.env'
        if not os.path.exists(env):
            with open(env, 'w') as file:
                for var in options:
                    file.write(f'{var}=\n')
        print(f'Created config {os.path.abspath(env)}')

    def do_reloadenv(self, _):
        load_dotenv()

    def do_restore(self, _):
            id = _.argv[1]
            target = _.argv[2]
            restic.restore(snapshot_id=id,target_dir=target)

# Main
def main():    
    prompt().cmdloop()

if __name__ == "__main__":
    main()
